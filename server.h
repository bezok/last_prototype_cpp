#ifndef SERVER_H
#define SERVER_H

#include <iostream>

#include "world/world.h"
#include "network/network.h"
#include <python3.5/Python.h>
#include <memory>

class World;
class Network;


class Server
{
public:
    Server();

    void main_loop();
    void start();
    void open_python();

    std::weak_ptr<World> get_world();
    std::weak_ptr<Network> get_network();

protected:
    std::chrono::system_clock::time_point current_time;
    std::shared_ptr<Network> network;
    std::shared_ptr<World> world;
    std::vector<std::thread> threads;
};

#endif // SERVER_H
