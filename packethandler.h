#ifndef PACKETHANDLER_H
#define PACKETHANDLER_H

#include "network.h"
#include "server.h"
#include "world/world.h"
#include <memory>
#include "unordered_map"
#include "json.hpp"
#include <math.h>
#include <queue>

using json = nlohmann::json;

class PacketHandler
{
public:
    PacketHandler(Server server);

protected:
    //function
    void make_new_connection_packet();
    void make_packet();

    json make_created_packet();
    json make_deleted_packet();
    json make_update_packet();

    //variables
    std::queue<unsigned int> *created_entities;
    std::queue<unsigned int> *deleted_entities;
    Entities *entities;                 //typedef from world.h
    std::unique_ptr<Network> network;
    std::unique_ptr<World> world;
};

#endif // PACKETHANDLER_H



/*
 *  1 - update
 *  2 - add
 *  3 - delete
 *
 */
