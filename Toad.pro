TEMPLATE = app
CONFIG += c++17
CONFIG -= app_bundle
CONFIG += thread

LIBS += \
       -lboost_system\

SOURCES += main.cpp \
    world/world.cpp \
    world/components/entity.cpp \
    world/components/creature.cpp \
    server.cpp \
    world/components/weapon.cpp \
    world/components/bullet.cpp \
    network/network.cpp \
    world/contactlistener.cpp \
    world/components/armor.cpp \
    network/client.cpp \
    world/components/human.cpp \
    world/components/item.cpp

HEADERS += \
    world/world.h \
    world/components/entity.h \
    world/components/creature.h \
    server.h \
    world/components/weapon.h \
    world/components/bullet.h \
    network/network.h \
    world/contactlistener.h \
    world/components/armor.h \
    network/client.h \
    world/components/human.h \
    world/components/item.h



DISTFILES += \
    network/test.html \
    script/human.py \
    script/arrow.py \
    script/bow.py \
    script/Network.py \
    script/sword.py

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../usr/lib/python3.5/config-3.5m-x86_64-linux-gnu/release/ -lpython3.5
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../usr/lib/python3.5/config-3.5m-x86_64-linux-gnu/debug/ -lpython3.5
else:unix: LIBS += -L$$PWD/../../../../usr/lib/python3.5/config-3.5m-x86_64-linux-gnu/ -lpython3.5

INCLUDEPATH += $$PWD/../../../../usr/include/python3.5m
DEPENDPATH += $$PWD/../../../../usr/include/python3.5m

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../usr/lib/x86_64-linux-gnu/release/ -lBox2D
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../usr/lib/x86_64-linux-gnu/debug/ -lBox2D
else:unix: LIBS += -L$$PWD/../../../../usr/lib/x86_64-linux-gnu/ -lBox2D

INCLUDEPATH += $$PWD/../../../../usr/include/Box2D
DEPENDPATH += $$PWD/../../../../usr/include/Box2D
