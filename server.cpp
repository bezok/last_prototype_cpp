#include "server.h"



Server::Server()
{
    std::cout << "Initialize server" << std::endl;
    this->open_python();
    world.reset(new World);
    network.reset(new Network(this));
}



void Server::main_loop()
{
    while(true)
    {
        this->current_time = std::chrono::system_clock::now();

        world.get()->update(this->current_time);
        network.get()->handle_packet(this->current_time);

#ifndef NDEBUG
        std::this_thread::sleep_for(std::chrono::seconds(2));
#else
        std::this_thread::sleep_for(std::chrono::milliseconds(32));
#endif
    }
}



void Server::start()
{

    threads.push_back(std::thread(([this]{main_loop();})));
    threads.push_back(std::thread(([this]{network.get()->start();})));

    for (auto it = threads.begin(); it != threads.end(); it++)
    {
        it->join();
    }
}



void Server::open_python()
{
    std::cout << "Open python interpreter" << std::endl;
    Py_Initialize();
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append(\"script/\")");
}



std::weak_ptr<Network> Server::get_network()
{
    return network;
}



std::weak_ptr<World> Server::get_world()
{
    return world;
}



