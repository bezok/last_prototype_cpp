
damping = 0.01

size_x = 1.0
size_y = 1.0
density = 1.0

category = 0x10



health = 50.0
health_regeneration = 0.1/60.0
max_health = 100.0



attack_speed = 1000
move_speed = 1000


agility = 100
durability = 100
strength = 100
vitality = 100

