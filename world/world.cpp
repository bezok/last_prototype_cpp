#include "world.h"



World::World()
    : b2_world(b2Vec2(0.0f, 0.0f))
{
    std::cout << "Initialize world" << std::endl;

    b2_world.SetAllowSleeping(true);
    b2_world.SetContactListener(&contact_listener);
    entity_id = 0;

    created_entities.reset(new std::queue<unsigned int>());
    deleted_entities.reset(new std::queue<unsigned int>());
    entities.reset(new Entities());

    this->entity_update_duration = std::chrono::duration<int, std::ratio<1, 1000>>(100);
}



World::~World()
{
    std::cout << "Delete world" << std::endl;
}



void World::add_entity(std::shared_ptr<Entity> entity)
{
#ifndef NDEBUG
    std::cout << "Add entity to world" << std::endl;
#endif

    entities.get()->insert(std::make_pair(entity->get_entity_id(), std::shared_ptr<Entity>(entity)));
    //done_action.push(std::make_tuple<std::string, unsigned int>("cc", entity.get()->get_entity_id()));
    //this->created_entities.get()->push(entity.get()->get_entity_id());
}



b2Body *World::create_body(b2BodyDef *body_def)
{
#ifndef NDEBUG
    std::cout << "b2_world create body" << std::endl;
#endif

    assert(body_def);

    return b2_world.CreateBody(body_def);
}



void World::delete_entity(unsigned int entity_id)
{
#ifndef NDEBUG
    std::cout << "Delete entity from world" << std::endl;
#endif

    if (entities.get()->find(entity_id)->second->get_body())
    {
        auto body = entities.get()->find(entity_id)->second->get_body();
        b2_world.DestroyBody(body);
        std::cout << "Delete entity from world" << std::endl;
    }

    entities.get()->erase(entity_id);
    done_action.push(std::make_tuple<std::string, unsigned int>("dc", static_cast<unsigned int>(entity_id)));
    //this->created_entities.get()->push(entity_id);
}



void World::start()
{
    std::cout << "Start world" << std::endl;

    //update();
}



void World::update(std::chrono::system_clock::time_point current_time)
{
#ifndef NDEBUG
    std::cout << "World update" << std::endl;
#endif

    this->current_time = current_time;

    if (this->entity_update_time - this->entity_update_duration  <= current_time)
    {
#ifndef NDEBUG
        std::cout << "  World update: entities" << std::endl;
#endif
        this->update_entities();
        this->entity_update_time = current_time;
    }

    if (world_update_time - std::chrono::duration<int, std::ratio<1, 100>>(32) <= current_time)
    {
#ifndef NDEBUG
        std::cout << "  World update: b2_world" << std::endl;
#endif

        try
        {
#ifndef NDEBUG
            this->b2_world.Step(1.0f, 6, 2);
#else
            this->b2_world.Step(1.0f/30.0f, 6, 2);
#endif

        }
        catch(...)
        {

        }

        this->world_update_time = current_time;
    }

}



void World::update_entities()
{
#ifndef NDEBUG
        std::cout << "Update entities" << std::endl;
#endif

    for (auto it = entities.get()->begin(); it != entities.get()->end(); it++)
    {
        it->second->update();
    }
}



std::tuple<std::string, unsigned int> World::get_done_action()
{
    if (done_action.size())
    {
        auto action = std::tuple<std::string, unsigned int>(done_action.front());
        done_action.pop();
        return action;
    }
    else
    {
        return std::tuple<std::string, unsigned int>("", 0);
    }
}



b2World World::get_b2_world()
{
    return b2_world;
}



std::weak_ptr<std::queue<unsigned int>> World::get_created_entities()
{
    return created_entities;
}



std::chrono::system_clock::time_point World::get_current_time()
{
    return current_time;
}



std::weak_ptr <std::queue<unsigned int>> World::get_deleted_entities()
{
    return deleted_entities;
}



std::weak_ptr<Entities> World::get_entities()
{
    return std::weak_ptr<Entities>(entities);
}



uint World::get_entity_id()
{
    return entity_id++;
}



std::weak_ptr<World> World::get_world()
{
    return world;
}
