#ifndef ARMOR_H
#define ARMOR_H

#include "entity.h"

class Armor : virtual public Entity
{
public:
    Armor(std::weak_ptr<World> world);

    void hit();
};

#endif // ARMOR_H
