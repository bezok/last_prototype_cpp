#ifndef CREATURE_H
#define CREATURE_H

#include <chrono>
#include <unordered_map>

#include <python3.5/Python.h>

#include "entity.h"
#include "weapon.h"

typedef std::chrono::duration<int, std::ratio<1, 1000000>> duration;


class Creature : public Entity
{
public:
    Creature(std::weak_ptr<World> world);

    virtual void update();

    void set_attack(uint entity_id);
    void set_move(int x, int y);

protected:
    // functions
    virtual void attack();
    virtual void hit(int damage);
    virtual void move();
    virtual void update_health();
    void set_creature(std::string script);


    // variables
    bool alive;
    float health;
    float health_regeneration;
    unsigned int max_health;
    duration attack_speed;
    duration move_speed;
    std::chrono::system_clock::time_point attack_time;
    std::chrono::system_clock::time_point move_time;
    unsigned int attack_to;
    b2Vec2 move_to;
    std::weak_ptr<Weapon> weapon;
    b2RevoluteJoint *hand;
};

#endif // CREATURE_H
