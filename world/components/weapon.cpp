#include "weapon.h"
#include "world/world.h"

Weapon::Weapon(std::weak_ptr<World> world, std::string script)
    : Entity(world)
{
    this->create_weapon(script);
}



Weapon::~Weapon()
{

}



void Weapon::attack(b2Vec2 vector)
{
    if (is_distance)
    {
        distance_attack(vector);
    }
    else
    {
        melee_attack(vector);
    }
}



void Weapon::create_weapon(std::string script)
{
    create_body(script);
    this->body->SetBullet(false);
    this->entity_type = "weapon";

    auto script_name = PyUnicode_FromString(script.c_str());
    auto module_name = PyImport_Import(script_name);

    is_distance = PyLong_AsLong(PyObject_GetAttrString(module_name, "is_distance"));
    damage = PyLong_AsLong(PyObject_GetAttrString(module_name, "damage"));
}



void Weapon::distance_attack(b2Vec2 vector)
{
    std::shared_ptr<Bullet> bullet(new Bullet(this->world, this->bullet_name));
    this->world.lock().get()->add_entity(bullet);

    float force_size = sqrt(pow(vector.x, 2) + pow(vector.y, 2));
    b2Vec2 force;
    force.x = (vector.x / force_size) * 10;
    force.y = (vector.y / force_size) * 10;
    bullet->fire(force);
}



void Weapon::melee_attack(b2Vec2 vector)
{

}



