#include "bullet.h"

Bullet::Bullet(std::weak_ptr<World> world, std::string script)
    : Entity(world)
{
    create_bullet(script);
}



void Bullet::create_bullet(std::__cxx11::string script)
{
    create_body(script);
    this->body->SetBullet(true);
    this->entity_type = "weapon";

    auto script_name = PyUnicode_FromString(script.c_str());
    auto module_name = PyImport_Import(script_name);

    damage = PyLong_AsLong(PyObject_GetAttrString(module_name, "damage"));
}



void Bullet::fire(b2Vec2 force)
{
    this->body->ApplyLinearImpulseToCenter(force, true);
}
