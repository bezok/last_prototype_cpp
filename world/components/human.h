#ifndef HUMAN_H
#define HUMAN_H

#include "creature.h"

class World;


class Human : public Creature
{
public:
    Human(std::weak_ptr<World> world);

    void hold(unsigned int entity_id);
    void set_weapon(std::string script);
    void release();

protected:
    void attack();
    void update_health();

    unsigned int agility;
    unsigned int durability;
    unsigned int hungry;
    unsigned int stamina;
    unsigned int strength;
    unsigned int vitality;

    std::tuple<std::weak_ptr<Entity>, b2Joint *> left_hand;
    std::tuple<std::weak_ptr<Entity>, b2Joint *> right_hand;
};

#endif // HUMAN_H
