#ifndef BULLET_H
#define BULLET_H

#include <python3.5/Python.h>
#include "Box2D/Box2D.h"
#include "entity.h"

class Bullet : public Entity
{
public:
    Bullet(std::weak_ptr<World> world, std::string script);

    void create_bullet(std::string script);

    void fire(b2Vec2 force);

protected:
    unsigned int damage;
};

#endif // BULLET_H
