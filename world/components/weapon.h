#ifndef WEAPON_H
#define WEAPON_H

#include <memory>
#include "entity.h"

#include "bullet.h"


class World;

class Weapon : virtual public Entity
{
public:
    Weapon(std::weak_ptr<World> world, std::string script);
    ~Weapon();

    void attack(b2Vec2 vector);

    void create_weapon(std::string script);

    void distance_attack(b2Vec2 vector);
    void melee_attack(b2Vec2 vector);

protected:
    std::string bullet_name;
    unsigned int damage;
    bool is_distance;
};

#endif // WEAPON_H
