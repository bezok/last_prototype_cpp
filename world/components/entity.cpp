#include "entity.h"
#include "../world.h"

Entity::Entity()
{
    this->entity_type = "entity";
}



Entity::Entity(std::weak_ptr<World> world)
    : world(world),
      body(NULL)
{
#ifndef NDEBUG
    std::cout << "Initialize entity" << std::endl;
#endif

    assert(world.lock().get());

    auto world_ptr = world.lock();
    this->entity_id = world_ptr->get_entity_id();
}



Entity::~Entity()
{
#ifndef NDEBUG
    std::cout << "Delete entity" << std::endl;
#endif

    //auto world_ptr = world.lock();

    //world_ptr.get()->get_b2_world().DestroyBody(body);
}



void Entity::create_body(std::string script)
{
#ifndef NDEBUG
    std::cout << "Create body" << std::endl;
#endif

    b2BodyDef body_def;
    body_def.type = b2_dynamicBody;
    body_def.userData = this;

    body_def.linearDamping = 0.01f;
    body_def.angularDamping = 0.01f;

    auto world_ptr = world.lock();
    assert(world_ptr.get());
    this->body = world_ptr.get()->create_body(&body_def);

    b2FixtureDef fixture_def;
    b2PolygonShape shape;
    float size_x = 1.0f;
    float size_y = 1.0f;

    shape.SetAsBox(size_x, size_y);
    fixture_def.shape = &shape;
    fixture_def.filter.categoryBits = 0;

    body->CreateFixture(&fixture_def);
}



void Entity::update()
{
#ifndef NDEBUG
    std::cout << "Entity update" << std::endl;
#endif
}



b2Body *Entity::get_body()
{
    return this->body;
}



unsigned int Entity::get_entity_id()
{
    return this->entity_id;
}



std::string Entity::get_type()
{
    return entity_type;
}
