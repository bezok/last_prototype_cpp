#ifndef ENTITY_H
#define ENTITY_H

#include <memory>
#include "Box2D/Box2D.h"
#include <iostream>

#include <Python.h>

class World;

class Entity
{
public:
    Entity();
    Entity(std::weak_ptr<World> world);
    ~Entity();

    virtual void update();

    b2Body *get_body();
    unsigned int get_entity_id();
    std::string get_type();

protected:
    virtual void create_body(std::string script);

    b2Body *body;
    unsigned int entity_id;
    std::weak_ptr<World> world;
    std::string entity_type;
};

#endif // ENTITY_H
