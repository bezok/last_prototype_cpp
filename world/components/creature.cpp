#include "creature.h"
#include "world/world.h"



Creature::Creature(std::weak_ptr<World> world)
    : Entity(world)
{
#ifndef NDEBUG
    std::cout << "Initialize creature" << std::endl;
#endif

    this->entity_type = "creature";

    set_creature("human");
    create_body("human");
    //set_weapon("sword");

    move_to = b2Vec2(500.0, 0.0);
}



void Creature::attack()
{
    /*
    auto world_ptr = world.lock();
    if (this->attack_time >= world_ptr.get()->get_current_time() + attack_speed)
    {
        hand->SetMotorSpeed(12);
    }
    */
}



void Creature::hit(int damage)
{
    health -= damage;

    if (this->health <= 0)
    {
        this->alive = false;
    }
}



void Creature::move()
{
#ifndef NDEBUG
    std::cout << "Creature move" << std::endl;
#endif

    if (this->move_time <= world.lock().get()->get_current_time() + move_speed)
    {
        b2Vec2 force(0, 0);

        if (static_cast<int>(move_to.x) != static_cast<int>(body->GetPosition().x))
        {
            force.x = move_to.x - this->body->GetPosition().x;

            if (force.x > 5.0f)
            {
                force.x = 5.0f;
            }
        }

        if (static_cast<int>(move_to.y) != static_cast<int>(body->GetPosition().y))
        {
            force.y = move_to.y - this->body->GetPosition().y;

            if (force.y > 5.0f)
            {
                force.y = 5.0f;
            }
        }

        this->body->ApplyForceToCenter(force, true);
    }
}



void Creature::update()
{
#ifndef NDEBUG
    std::cout << "Creature update" << std::endl;
#endif

    if (this->alive)
    {
        this->attack();
        this->move();
        this->update_health();
    }
}



void Creature::update_health()
{
    if (this->health < this->max_health)
    {
        this->health += this->health_regeneration;

        if (this->health > this->max_health)
        {
            this->health = this->max_health;
        }
    }
}



void Creature::set_creature(std::string script)
{
#ifndef NDEBUG
    std::cout << "Set creature" << std::endl;
#endif

    auto script_name = PyUnicode_FromString(script.c_str());
    assert(script_name);

    auto module_name = PyImport_Import(script_name);
    assert(module_name);

    health = PyLong_AsLong(PyObject_GetAttrString(module_name, "health"));
    health_regeneration = PyLong_AsLong(PyObject_GetAttrString(module_name, "health_regeneration"));
    max_health = PyLong_AsLong(PyObject_GetAttrString(module_name, "max_health"));

    auto atk_speed = PyLong_AsLong(PyObject_GetAttrString(module_name, "attack_speed"));
    auto mov_speed = PyLong_AsLong(PyObject_GetAttrString(module_name, "move_speed"));

    this->attack_speed = std::chrono::duration<int, std::ratio<1, 1000000>>(atk_speed);
    this->move_speed = std::chrono::duration<int, std::ratio<1, 1000000>>(mov_speed);
}



void Creature::set_attack(unsigned int id)
{
    attack_to = id;
}



void Creature::set_move(int x, int y)
{
    move_to.x = x;
    move_to.y = y;
}
