#include "human.h"
#include "world/world.h"

Human::Human(std::weak_ptr<World> world)
    : Creature(world)
{
#ifndef NDEBUG
    std::cout << "Initialize human" << std::endl;
#endif

}



void Human::attack()
{

}



void Human::hold(unsigned int entity_id)
{

}



void Human::update_health()
{
    if (this->health < this->durability)
    {
        if (this->hungry)
        {
            this->health += this->vitality;
            this->hungry--;
            this->vitality += 0.001;
        }

        if (this->health > this->durability)
        {
            this->health = this->durability;
        }
    }
    else
    {
        if (this->hungry)
        {
            this->durability += 0.01;
        }
    }
}



void Human::set_weapon(std::string script)
{
#ifndef NDEBUG
    std::cout << "Set weapon" << std::endl;
#endif

    std::shared_ptr<Weapon> weapon(new Weapon(world, script));
    b2RevoluteJointDef joint_def;
    joint_def.bodyA = body;
    joint_def.bodyB = weapon.get()->get_body();
    joint_def.collideConnected = false;
    joint_def.enableMotor = true;
    joint_def.maxMotorTorque = 20;
    joint_def.motorSpeed = 0;
    hand = (b2RevoluteJoint*)world.lock().get()->get_b2_world().CreateJoint(&joint_def);
}



void Human::release()
{

}
