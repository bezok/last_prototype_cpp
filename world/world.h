#ifndef WORLD_H
#define WORLD_H

#include <iostream>

#include <chrono>
#include <memory>
#include <thread>
#include <unordered_map>
#include <queue>
#include <tuple>
#include "Box2D/Box2D.h"

#include "components/entity.h"
#include "contactlistener.h"

#include <mutex>


class Entity;
class Human;

// typedefs
typedef std::unordered_map<unsigned int, std::shared_ptr<Entity>> Entities;
typedef std::shared_ptr<std::queue<unsigned int>> shared_queue;



class World
{
public:
    World();
    ~World();

    void add_entity(std::shared_ptr<Entity> entity);
    b2Body *create_body(b2BodyDef *body_def);
    void delete_entity(unsigned int entity_id);

    void start();

    void update(std::chrono::system_clock::time_point current_time);
    void update_entities();

    std::tuple<std::string, unsigned int> get_done_action();
    b2World get_b2_world();
    std::weak_ptr<std::queue<unsigned int>> get_created_entities();
    std::chrono::system_clock::time_point get_current_time();
    std::weak_ptr<std::queue<unsigned int>> get_deleted_entities();
    std::weak_ptr<Entities> get_entities();
    unsigned int get_entity_id();
    std::weak_ptr<World> get_world();

private:
    ContactListener contact_listener;
    b2World b2_world;
    std::chrono::system_clock::time_point current_time;
    std::queue<std::tuple<std::string, unsigned int> > done_action;
    std::chrono::duration<int, std::ratio<1, 1000>> entity_update_duration;
    std::chrono::system_clock::time_point entity_update_time;
    std::shared_ptr<Entities> entities;
    unsigned int entity_id;
    std::shared_ptr<World> world;
    std::chrono::system_clock::time_point world_update_time;
    shared_queue created_entities;
    shared_queue deleted_entities;
};

#endif // WORLD_H
