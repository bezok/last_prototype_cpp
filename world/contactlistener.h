#ifndef CONTACTLISTENER_H
#define CONTACTLISTENER_H

#include "Box2D/Box2D.h"


class ContactListener : public b2ContactListener
{
public:
    ContactListener();

    void BeginContact(b2Contact *contact);

    void creature_weapon(b2Contact *contact);

};

#endif // CONTACTLISTENER_H
