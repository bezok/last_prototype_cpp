#include "server.h"
#include "network/network.h"
#include <chrono>
#include <thread>

#include <python3.5/Python.h>
#include <iostream>

//#define NDEBUG

int main()
{
    Server server;
    server.start();
}
