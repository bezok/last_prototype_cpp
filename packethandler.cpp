#include "packethandler.h"

PacketHandler::PacketHandler(Server server)
    : network(server.get_network()),
      world(server.get_world())
{
    this->created_entities = world.get()->get_created_entities();
    this->deleted_entities = world.get()->get_deleted_entities();
    this->entities = world.get()->get_entities();
}



void PacketHandler::make_new_connection_packet()
{

}



void PacketHandler::make_packet()
{
    json data_to_packet;
    data_to_packet['n'] = make_created_packet();
    data_to_packet['d'] = make_deleted_packet();
    data_to_packet['u'] = make_update_packet();

}



json PacketHandler::make_created_packet()
{
    if (created_entities->size() != 0)
    {
        json new_data;

        while(!created_entities->empty())
        {
            json data;
            unsigned int entity_id = created_entities->front();
            data['id'] = entity_id;
            auto entity = entities->find(entity_id);
            data['x'] = roundf(entity->second->get_body()->GetPosition().x * 100) / 100;
            data['y'] = roundf(entity->second->get_body()->GetPosition().y * 100) / 100;
            data['a'] = roundf(entity->second->get_body()->GetAngle() * 100) / 100;
            new_data.push_back(data);
        }
        return new_data;
    }
    else
    {
        return 0;
    }
}



json PacketHandler::make_deleted_packet()
{
    if (deleted_entities->size() != 0)
    {
        json deleted_data;

        while(!deleted_entities->empty())
        {
            json data;
            unsigned int entity_id = deleted_entities->front();
            data['id'] = entity_id;
            deleted_data.push_back(data);
        }
        return deleted_data;
    }
    else
    {
        return 0;
    }
}



json PacketHandler::make_update_packet()
{
    json update_data;

    for (auto it = entities->begin(); it != entities->end(); it++)
    {
        json data;
        auto body = it->second->get_body();
        data['id'] = it->second->get_entity_id();
        data['x'] = roundf(body->GetPosition().x * 100) / 100;
        data['y'] = roundf(body->GetPosition().y * 100) / 100;
        data['a'] = roundf(body->GetAngle() * 100) / 100;

        update_data.push_back(data);
    }

    return update_data;
}
