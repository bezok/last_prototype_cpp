#ifndef CLIENT_H
#define CLIENT_H

#include "websocketpp/server.hpp"
#include <queue>
#include "json.hpp"

using json = nlohmann::json;

class Client
{
public:
    Client();

    void add_to_send(std::string packet);
    void add_entity(unsigned int entity_id);

    std::vector<unsigned int> get_entities();
    std::queue<std::string> get_packets_to_send();

    void pop();

protected:
    std::vector<unsigned int> entities;
    std::queue<std::string> packets_to_send;
    std::queue<std::string> received_packet;
};

#endif // CLIENT_H
