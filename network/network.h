#ifndef NETWORK_H
#define NETWORK_H

#include "world/components/creature.h"

#include <iostream>

#include <memory>
#include <python3.5/Python.h>
#include "client.h"
#include "world/components/human.h"
#include "world/world.h"
#include "server.h"

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>


using json = nlohmann::json;

typedef websocketpp::server<websocketpp::config::asio> Websocket;

class Server;



class Network
{
public:
    Network(Server *server);

    void handle_packet(std::chrono::system_clock::time_point current_time);

    std::string make_packet();
    json make_packet_created(unsigned int entity_id);
    json make_packet_done();
    json make_packet_deleted(unsigned int entity_id);
    json make_packet_update();

    void on_close(websocketpp::connection_hdl hdl);
    void on_message(websocketpp::connection_hdl hdl, Websocket::message_ptr msg);
    void on_open(websocketpp::connection_hdl hdl);
    void run();
    void send_packets();
    void send_to_clients(std::string packet);
    void start();

protected:
    std::map<websocketpp::connection_hdl, Client, std::owner_less<websocketpp::connection_hdl>> clients;
    std::weak_ptr<std::queue<unsigned int>> created_entities;
    std::weak_ptr<std::queue<unsigned int>> deleted_entities;
    std::weak_ptr<Entities> entities;
    std::chrono::system_clock::time_point packet_handle_time;
    std::weak_ptr<World> world;
    Websocket websocket;
};

#endif // NETWORK_H
