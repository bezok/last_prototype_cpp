#include "network.h"



Network::Network(Server *server)
    : world(server->get_world())
{
    std::cout << "Initialize network" << std::endl;

    entities = world.lock().get()->get_entities();
    created_entities = world.lock().get()->get_created_entities();
    deleted_entities = world.lock().get()->get_deleted_entities();

    assert(entities.lock().get());
    assert(created_entities.lock().get());
    assert(deleted_entities.lock().get());
}



void Network::handle_packet(std::chrono::system_clock::time_point current_time)
{
#ifndef NDEBUG
    std::cout << "Handle packets" << std::endl;
#endif

    if (packet_handle_time - std::chrono::duration<int, std::ratio<1, 100>>(32) <= current_time)
    {
        std::string packet = make_packet();

        if (packet.size() != 0)
        {
            send_to_clients(packet);
            //try
            //{
                send_packets();
            //}
            //catch(...)
            //{

            //}
        }

        this->packet_handle_time = current_time;
    }
}



std::string Network::make_packet()
{
#ifndef NDEBUG
    std::cout << "Make packet" << std::endl;
#endif

    json packet_data;
    packet_data["a"] = make_packet_done();
    packet_data["u"] = make_packet_update();

    if (packet_data.size())
    {
        std::string packet = packet_data.dump();
        return packet;
    }
    else
    {
        return "";
    }

}



json Network::make_packet_done()
{
#ifndef NDEBUG
    std::cout << "  Make packet action done" << std::endl;
#endif

    json actions;

    while(true)
    {
        auto done_action = world.lock().get()->get_done_action();
        if (std::get<1>(done_action))
        {
            json action;

            if (std::get<0>(done_action) == "cc")
            {
                action = make_packet_created(std::get<1>(done_action));
                actions.push_back(action);
            }
            else if (std::get<0>(done_action) == "dc")
            {
                action = make_packet_deleted(std::get<1>(done_action));
                actions.push_back(action);
            }
            else
            {

            }
        }
        else
        {
            return actions;
        }
    }
}



json Network::make_packet_created(unsigned int entity_id)
{
#ifndef NDEBUG
    std::cout << "      Make packet created" << std::endl;
#endif

    json data;
    data["a"] = "cc";

    auto entity = entities.lock().get()->find(entity_id);

    data["id"] = entity_id;
    data["x"] = entity->second->get_body()->GetPosition().x;
    data["y"] = entity->second->get_body()->GetPosition().y;
    data["angle"] = entity->second->get_body()->GetAngle();
    return data;
}



json Network::make_packet_deleted(unsigned int entity_id)
{
#ifndef NDEBUG
    std::cout << "      Make packet deleted" << std::endl;
#endif

    json data;
    data["a"] = "dc";
    data["id"] = entity_id;
    return data;
}



json Network::make_packet_update()
{
#ifndef NDEBUG
    std::cout << "  Make packet update" << std::endl;
#endif

    json update_data;

    for (auto it = entities.lock().get()->begin(); it != entities.lock().get()->end(); it++)
    {
        if (it->second->get_body())
        {
        json data;
        auto body = it->second->get_body();
        data["id"] = it->second->get_entity_id();
        data["x"] = roundf(body->GetPosition().x * 100.00) / 100.00;
        data["y"] = roundf(body->GetPosition().y * 100.00) / 100.00;
        data["a"] = roundf(body->GetAngle() * 100.00) / 100.00;
        data["type"] = it->second->get_type();

        update_data.push_back(data);
        }
    }

    return update_data;
}



void Network::on_close(websocketpp::connection_hdl hdl)
{
#ifndef NDEBUG
    std::cout << "Connection closed" << std::endl;
#endif

    auto entities = clients[hdl].get_entities();
    for (auto it = entities.begin(); it != entities.end(); it++)
    {
        world.lock().get()->delete_entity(*it);
        //std::cout << *it << std::endl;
    }
    clients.erase(hdl);

}



void Network::on_message(websocketpp::connection_hdl hdl, Websocket::message_ptr msg)
{
    std::cout << msg->get_payload() << std::endl;
}



void Network::on_open(websocketpp::connection_hdl hdl)
{
#ifndef NDEBUG
    std::cout << "Connection opened" << std::endl;
#endif

    clients[hdl] = Client();

    //auto creature = std::make_shared<Creature>(world);
    //auto creature = new Creature(world);
    auto human = std::shared_ptr<Human>(new Human(world));

    world.lock().get()->add_entity(human);

    clients[hdl].add_entity(human.get()->get_entity_id());
}



void Network::run()
{
    std::cout << "Run websocket" << std::endl;

    websocket.init_asio();

    /*
    auto name = PyUnicode_FromString("network");
    auto module = PyImport_Import(name);
    auto port = PyObject_GetAttrString(module, "port");

    websocket.listen(PyLong_AsLong(port));
*/

    websocket.clear_access_channels(websocketpp::log::alevel::all);

    websocket.set_reuse_addr(true);
    websocket.listen(53222);

    websocket.set_close_handler(websocketpp::lib::bind(&Network::on_close, this, websocketpp::lib::placeholders::_1));
    websocket.set_fail_handler(websocketpp::lib::bind(&Network::on_close, this, websocketpp::lib::placeholders::_1));
    websocket.set_message_handler(websocketpp::lib::bind(&Network::on_message, this, websocketpp::lib::placeholders::_1, websocketpp::lib::placeholders::_2));
    websocket.set_open_handler(websocketpp::lib::bind(&Network::on_open, this, websocketpp::lib::placeholders::_1));

    websocket.start_accept();
    websocket.run();
}



void Network::send_packets()
{
#ifndef NDEBUG
    std::cout << "Send packet" << std::endl;
#endif

    for (auto it = clients.begin(); it != clients.end(); it++)
    {
        auto packet = it->second.get_packets_to_send().front();
        it->second.pop();
        websocket.send(it->first, packet, websocketpp::frame::opcode::text);
    }
}



void Network::send_to_clients(std::string packet)
{
#ifndef NDEBUG
    std::cout << "Send to clients" << std::endl;
#endif

    for (auto it = clients.begin(); it != clients.end(); it++)
    {
        it->second.add_to_send(packet);
    }
}



void Network::start()
{
    std::cout << "Start network" << std::endl;

    //std::thread handle_packet_thread = std::thread(&Network::handle_packet, this);
    run();
    //handle_packet_thread.join();
}




