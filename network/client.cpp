#include "client.h"

Client::Client()
{

}



void Client::add_to_send(std::string packet)
{
    this->packets_to_send.push(packet);
}



void Client::add_entity(unsigned int entity_id)
{
    entities.push_back(entity_id);
}



std::vector<unsigned int> Client::get_entities()
{
    return entities;
}



std::queue<std::string> Client::get_packets_to_send()
{
    return packets_to_send;
}



void Client::pop()
{
    packets_to_send.pop();
}
